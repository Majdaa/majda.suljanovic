$( document ).ready(function() {
    $(".navigation").html('<!-- Dropdown Structure -->' +
    '<ul id="dropdown1" class="dropdown-content">' +
        '<li><a href="project1.html" class="black-text">Project Management</a></li>' +
        '<li><a href="project2.html" class="black-text">2D and 3D Design</a></li>' +
        '<li><a href="project3.html" class="black-text">Laser Cutting</a></li>' +
        '<li><a href="project4.html" class="black-text">3D Printing</a></li>' +
        '<li><a href="project5.html" class="black-text">Electronics Production</a></li>' +
        '<li><a href="project6.html" class="black-text">Electronics Design</a></li>' +
        '<li><a href="project7.html" class="black-text">Embedded Programming</a></li>' +
        '<li><a href="project8.html" class="black-text">Input Devices</a></li>' +
        '<li><a href="project9.html" class="black-text">Output Devices</a></li>' +
        '<li><a href="project10.html" class="black-text">Final Project</a></li>' +
      '</ul> <nav class="white" role="navigation"> <div class="nav-wrapper container"> <ul class="right hide-on-med-and-down"> <li><a href="index.html" class="black-text">Home</a></li> <!-- Dropdown Trigger -->' +
                '<li><a class="dropdown-trigger black-text" href="#!" data-target="dropdown1">Projects<i class="material-icons right">arrow_drop_down</i></a></li> </ul> <ul id="nav-mobile" class="sidenav">' +
                '<li><a href="index.html" class="black-text">Home</a></li>' +
                '<li><a href="project1.html" class="black-text">Project Management</a></li>' +
                '<li><a href="project2.html" class="black-text">2D and 3D Design</a></li>' +
                '<li><a href="project3.html" class="black-text">Laser Cutting</a></li>' +
                '<li><a href="project4.html" class="black-text">3D Printing</a></li>' +
                '<li><a href="project5.html" class="black-text">Electronics Production</a></li>' +
                '<li><a href="project6.html" class="black-text">Electronics Design</a></li>' +
                '<li><a href="project7.html" class="black-text">Embedded Programming</a></li>' +
                '<li><a href="project8.html" class="black-text">Input Devices</a></li>' +
                '<li><a href="project9.html" class="black-text">Output Devices</a></li>' +
                '<li><a href="project10.html" class="black-text">Final Project</a></li>' +
                '</ul> <a href="#" data-target="nav-mobile" class="sidenav-trigger"><i class="material-icons black-text">menu</i></a> </div> </nav>');
                
    $(".dropdown-trigger").dropdown();
    $('.sidenav').sidenav();

    $('.parallax').parallax();
    $('.materialboxed').materialbox();

    $(".projectimage").mouseover(function() {
        var id = $(this).attr('id');
        $("#" + id + " img").css({"cursor": "pointer", "-webkit-filter": "grayscale(0%)", "filter": "grayscale(0%)", "transition-duration": "0.2s"});
    });

    $(".projectimage").mouseleave(function() {
        var id = $(this).attr('id');
        $("#" + id + " img").css({"-webkit-filter": "grayscale(100%)", "filter": "grayscale(100%)", "transition-duration": "0.2s"});
    });

    
});

